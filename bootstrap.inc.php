<?php

/*
 * This file is part of the loops/autoloader package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Autoloader;

require __DIR__.'/src/Psr40.php';

// add prefix and root for this package
\Loops\Autoloader\Psr40::getInstance()->add( __NAMESPACE__ , __DIR__.'/src' );